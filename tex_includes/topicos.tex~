\begin{comment}
\textbf{Problemática}
O objetivo dessa etapa é:
\begin{itemize}
   \item
   Contexto da furação na industria (processo comum na industria porém complexo. Explicar pq é complexo);
   \item
   Apresentar o processo de furação ao leitor (OK);
   \item 
   Justificar as altas temperaturas geradas no processo;
   \item
   Pegar o gancho da temperatura e justificar o objetivo do trabalho;
   \item
   Abordar a justificativa do do ponto de vista térmico e do ponto de vista do fluído. Mostrar referências sobre eficiência custo e meio ambiente  
   (OK);
   \item
   Fazer as hipóteses gerais.
   \item
   Mencionar as condições de corte que serão experimentadas para analisar a sensibilidade de \Hfluido\ com relação a essas condições (OK);
   \item
   Mencionar a geometria complexa da broca;
   \item
   Mencionar características do processo;
\end{itemize}
\end{comment}
\newpage
\noindent\textbf{Estado da arte}

O objetivo dessa etapa é:
\begin{itemize}
   \item
   Contexto da furação na indústria (processo comum, porém complexo);
   \item
   Apresentar o processo de furação:
   \begin{itemize}
      \item
      O que é?
      \item
      Como ele é?
      \item
      Qual a utilidade?
   \end{itemize}
   \item
   Mencionar as altas temperaturas geradas no processo;
   \item
   Relacionar as altas temperaturas com:
   \begin{itemize}
      \item
      Desgaste;
      \item
      Qualidade superfícial;
      \item
      Calor que vai para a peça;
   \end{itemize}
   \item
   Para minimizar esses efeitos é usado fluído de corte;
   \item
   Apresentar o fluído de corte:
   \begin{itemize}
      \item
      O que é?
      \item
      Quais as funções?
      \item
      Como atua?
      \item
      Entra ou não na interface de corte?
   \end{itemize}
   \item
   Modo de aplicação: são geralmente externos porém dificilmente o fluído chegará na interfce de corte, por isso adota-se refrigeração interna.
   Porém nem todas as máquinas possuem refrigeração no eixo arvore;
   \item
   Entretanto o fluído de corte é prejudicial (Citar as fontes que dizem isso sobre custo e tal);
   \item
   Dai entra o MQL:
   \begin{itemize}
      \item
      O que é?
      \item
      Onde é usado?
      \item
      Existem processos onde ele é melhor?
   \end{itemize}
   \item
   Mas continua sendo importante descobrir a temperatura na ponta da broca;
   \item
   O ideal seria fornecer fluído de corte com o único objetivo de refrigerar a ferramenta;
   \item
   Denkena tentou otimizar o a utilização de fluído de corte na	prática
   \item
   Aqui entra a simulação da broca:
   \begin{itemize}
      \item
      Um simulou a temperatura;
      \item
      Outro o fluxo de calor;
   \end{itemize}
   \item
   Geralmente as simulações são a seco (h do ar);
   \item
   Existem alguns trabalhos para determinar o h para superfícies simples, porém a broca possuí superfície complexa;
   \item
   Para a furação, pelo meu conhecimento, não existe trabalho para levantamento de h;
   \item
   O fato de que a broca ter superfície complexa e girar em torno de um eixo, torna o experimento da temperatura complexo;
   \item
   O objetivo do trabalho é o de determinar o valor do coeficiente de convecção do fluído para algumas condições de corte específicas a fim 
   de      
   determinar a influência de cada uma dessas 	condições no valor do h;
   \item
   Para isso será mostrado alguns métodos de determinação do h;
   \item	
   E posteriormente aplicado o método escolhido ao modelo térmico do processo de furação;
   \item
   Um aparato experimental será construido com o intuíto de facilitar a medição de várias grandezes durante os experimentos;
\end{itemize}

\newpage
\noindent\textbf{Embassamento teórico}

O objetivo dessa etapa é:
\begin{itemize}
   \item
   Apresentar os métodos que podem ser utilizados para encontrar o \Hfluido; (OK)
   \item
   Justificar porque cada um pode ou não ser aplicado e as dificuldades de aplicação; (OK)
   \item
   Apresentar a equação algébrica que governa o valor de \Hfluido\ e que deve ser resolvida pelo método escolhido; (OK)
   \item
   Apresentar o roteiro do método utilizado. O problema deve ser dividido em parte experimental e parte numérica; (OK)
   \item
   Apresentar o modelo térmico do problema; (OK)
   \item
   Apresentar as simplificações do modelo térmico visando a utilização do método escolhido; (OK)
   \item
   Apresentar onde cabe a parte experimental e onde cabe a parte numérica; (OK)
   \item
   Apresentar a equação que governara o modelo numérico; (OK)
   \item
   Apresentar a sequência de resolução do acoplamento experimental numérico; (OK)
\end{itemize}
Adicionar os termos Cisol e Carpinca a algum lugar aqui.

\newpage
\noindent\textbf{Aparato experimental}
	
O objetivo dessa etapa é:
\begin{itemize}
   \item
   Apresentar o aparato experimental;
   \item
   Apresentar o setup experimental; porque escolher a água?
   \item
   Apresentar o procedimento experimental;
   \item
   Apresentar o sistema de aquisição de temperaturas;
   \item
   Específicar os tipos de termopares;
   \item
   Especificar multimetro, paquimetro, tacometro.
\end{itemize}

\newpage
\noindent\textbf{Modelamento numérico}

O objetivo dessa etapa é:

\begin{comment}
\begin{itemize}
   \item
   Para encontrar \Tbrocafluido, apresentar a equação de governo numérica novamente;
   \item
   Comparar com a do \textit{software OpenFoam} (Solver utilizado) e fazer as simplificações necessárias na abordagem para ficar alinhada ao 
   trabalho;
   \item
   Condições de contorno numéricas;
   \item
   Apresentar o modelo da broca feito no \textit{software OnShape};
   \item
   Normas geométricas (como foi feito);
   \item
   Falar sobre a malha feita no \textit{software Salome};
   \item
   Apresentar a metodologia de análise no \textit{software Paraview}.
\end{itemize}
\end{comment}
