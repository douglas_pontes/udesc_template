\begin{figure}[!h]
\legenda{Temperatura da base (\Tbase) em função da vazão do fluido (\lfluido) para várias rotação da broca (\Nbiqueira).}
\centering
\begin{tikzpicture}
\begin{axis}[
             axis lines = left,
             title={\Tbase\ $X$ \lfluido},
             xlabel={\lfluido\ \unitvazao},
             ylabel={Temperatura da base \Tbase\ \unittemperatura},
             xmin=0.1,
             xmax=0.6,
             ymin=40,
             ymax=70,
             ytick={40,45,50,55,60,65,70},
             y tick label style={/pgf/number format/1000 sep=},
             xtick={0.1,0.2,0.3,0.4,0.5,0.6},
             legend pos=north west,
             ymajorgrids=true,
             grid style=dashed,
             height=8.5cm,
             width=15.4cm,
]

\addplot[
         color=blue,
         mark=square
] table {resultados/graficos/temperatura_da_base_por_vazao/temp_por_vaz_n_0.dat};
\label{vazao:n_0}

\addplot[ 
         color=red, 
         mark=triangle
] table {resultados/graficos/temperatura_da_base_por_vazao/temp_por_vaz_n_345.dat};
\label{vazao:n_345}

\addplot[
         color=green,
         mark=otimes
] table {resultados/graficos/temperatura_da_base_por_vazao/temp_por_vaz_n_365.dat};
\label{vazao:n_365}

\addplot[
         color=purple, 
         mark=diamond
] table {resultados/graficos/temperatura_da_base_por_vazao/temp_por_vaz_n_385.dat};
\label{vazao:n_385}

\end{axis}
\node [draw,fill=white] at (rel axis cs: 0.65,0.85) {\shortstack[l]{
\tiny\ref{vazao:n_0} \Nbiqueira\ $=0$ \unitrotacao \\
\tiny\ref{vazao:n_345} \Nbiqueira\ $=345$ \unitrotacao \\
\tiny\ref{vazao:n_365} \Nbiqueira\ $=365$ \unitrotacao  \\
\tiny\ref{vazao:n_385} \Nbiqueira\ $=385$ \unitrotacao 
               }
};

\end{tikzpicture}
\label{t_base:vazao_rotacao}
\fonteAutor
\end{figure}
