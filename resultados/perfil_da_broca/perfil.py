#!/usr/bin/python3

import math
import sys
import json
import os

from primitives import Broca, AchaParametrosSecundarios

##########################################################
##########################################################
# Dados de entrada da broca
W = 4.5  # Espessura da ponta [mm]
L = 140  # Comprimento da helice [mm]
D = 20  # Diametro da broca [mm]
P = 118  # Angulo da ponta da broca [graus]
l = 12.7  # Largura ortogonal da helice [mm] 12.7
a = 0  # Centro da broca na coordenada x [mm]
b = 0  # Centro da broca na coordenada y [mm]
# Configuracao do grafico
div_pri = 78  # Numero de divisoes do grafico para a curva da aresta primitiva
div_sec = 78  # Numero de divisoes do grafico para a curva da aresta secundaria
div_flanc = 100  # Numero de divisoes do grafico para a curva do flanco
# File names
aresta_pr_a_file_name = "aresta_de_corte_primaria_a.dat"
aresta_pr_b_file_name = "aresta_de_corte_primaria_b.dat"
aresta_sec_a_file_name = "aresta_de_corte_secundaria_a.dat"
aresta_sec_b_file_name = "aresta_de_corte_secundaria_b.dat"
flanco_a_file_name = "flanco_a.dat"
flanco_b_file_name = "flanco_b.dat"
##########################################################
##########################################################

broca_01 = Broca()
broca_01.add_diametro(D)
broca_01.add_espessura_da_ponta(W)
broca_01.add_comprimento_de_helice(L)
broca_01.add_angulo_da_ponta(P)
broca_01.add_largura_da_helice(l)
broca_01.add_centro_da_broca_x(a)
broca_01.add_centro_da_broca_y(b)
broca_01.pre_processa_geometria()
broca_01.faz_aresta_primaria(div_pri)
broca_01.pos_ponta_aresta_primaria()
broca_01.pos_ponta_aresta_secundaria()
broca_01.angulo_minimo_do_flanco()
broca_01.angulo_maximo_do_flanco()
broca_01.faz_flancos(div_flanc)
broca_01.faz_aresta_secundaria(div_sec)
broca_01.exporta_curva(
                       aresta_pr_a_file_name,
                       aresta_pr_b_file_name,
                       'primaria'
)
broca_01.exporta_curva(
                       flanco_a_file_name,
                       flanco_b_file_name,
                       'flanco'
)
broca_01.exporta_curva(
                       aresta_sec_a_file_name,
                       aresta_sec_b_file_name,
                       'secundaria'
)
