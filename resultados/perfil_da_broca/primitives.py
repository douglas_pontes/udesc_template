#!/usr/bin/python3
"""
"""
import math


class Broca:
    """Classe Broca"""

    D = None
    """Diametro da broca [mm]"""

    W = None
    """Espessura da ponta [mm]"""

    L = None
    """Comprimento da helice [mm]"""

    P = None
    """Angulo da ponta da broca [graus]"""

    l = None
    """Largura ortogonal da helice [mm] 12.7 """

    t = None
    """Metade da espessura da ponta [mm]"""

    ro = None
    """Raio da broca [mm]"""

    p = None
    """Meio angulo da ponta da broca [radianos]"""

    h = None
    """Angulo de helice [radianos]"""

    centro_x = None
    """Centro da broca em x [mm]"""

    centro_y = None
    """Centro da broca em y [mm]"""

    curva_primaria = None
    """Vetor com os valores de x e y da aresta primaria"""

    curva_secundaria = None
    """Vetor com os valores de x e y da aresta secundaria"""

    flanco = None
    """Vetor com os valores de x e y do flanco"""

    pos_A0 = None
    """Posicao da aresta A0"""

    pos_B0 = None
    """Posicao da aresta B0"""

    pos_C0 = None
    """Posicao da aresta C0"""

    angulo_min_do_flanco = None
    """Angulo minimo do flanco da broca"""

    angulo_max_do_flanco = None
    """Angulo maximo do flanco da broca"""

    angulo_min_da_aresta_secundaria = None
    """Angulo minimo da aresta secundaria da broca"""

    angulo_max_da_aresta_secundaria = None
    """Angulo maximo da aresta secundaria da broca"""

    def __init__(self):
        """Inicializa o objeto"""
        self.D = 0
        self.W = 0
        self.L = 0
        self.P = 0
        self.l = 0
        self.t = 0
        self.ro = 0
        self.p = 0
        self.h = 0
        self.centro_x = 0
        self.centro_y = 0
        self.centro_curva_secundaria_x = 3.017491
        self.centro_curva_secundaria_y = 8.4938194
        self.raio_curva_secundaria = 6.9347338
        self.curva_primaria = []
        self.curva_secundaria = []
        self.flanco = []
        self.pos_A0 = {'x': 0, 'y': 0}
        self.pos_B0 = {'x': 0, 'y': 0}
        self.pos_C0 = {'x': 0, 'y': 0}
        self.angulo_min_do_flanco = 0
        self.angulo_max_do_flanco = 0
        self.angulo_min_da_aresta_secundaria = 0
        self.angulo_max_da_aresta_secundaria = 0

    def add_diametro(self, D):
        """Adiciona o diametro da broca"""
        self.D = D
        return

    def add_espessura_da_ponta(self, W):
        """Adiciona a espessura da ponta da broca"""
        self.W = W
        return

    def add_comprimento_de_helice(self, L):
        """Adiciona o comprimento da helice da broca"""
        self.L = L
        return

    def add_angulo_da_ponta(self, P):
        """Adiciona o angulo da ponta da broca"""
        self.P = P
        return

    def add_largura_da_helice(self, l):
        """Adiciona largura da helice da broca"""
        self.l = l
        return

    def add_centro_da_broca_x(self, centro_x):
        """Adiciona centro em x da broca"""
        self.centro_x = centro_x
        return

    def add_centro_da_broca_y(self, centro_y):
        """Adiciona centro em y da broca"""
        self.centro_y = centro_y
        return

    def pre_processa_geometria(self):
        """Pre processa a geometrica basica da broca"""
        self.t = (self.W)/2
        self.ro = (self.D)/2
        self.p = ((self.P)*math.pi)/360
        self.h = math.atan((math.pi*(self.D))/(self.L))
        self.pos_C0['y'] = self.t
        return

    def faz_aresta_primaria(self, div):
        """Faz os pontos da aresta primaria da broca"""
        r = self.t
        incremento = (self.ro-self.t)/div
        while (r < self.ro+incremento):
            aresta = self._equacao_da_aresta_primaria(r)
            self.curva_primaria.append([aresta['x'], aresta['y']])
            r = r+incremento
        return

    def faz_aresta_secundaria(self, div1):
        """Faz os pontos da aresta secundaria da broca"""
        self._determina_coeficientes_curva_secundaria()
        angulo_min = self._angulo_minimo_curva_secundaria()
        angulo_max = self._angulo_maximo_curva_secundaria()
        angulo = angulo_min
        incremento = (angulo_max-angulo_min)/div1
        cont = 0
        while (angulo <= angulo_max)or(cont <= div1):
            curva = self._equacao_da_aresta_secundaria(angulo)
            self.curva_secundaria.append([curva['x'], curva['y']])
            angulo = angulo+incremento
            cont = cont+1
        return

    def faz_flancos(self, div1):
        """Faz os pontos do flanco da broca"""
        angulo = self.angulo_min_do_flanco
        incremento = (self.angulo_max_do_flanco-self.angulo_min_do_flanco)/div1
        cont = 0
        while (angulo <= self.angulo_max_do_flanco)or(cont <= div1):
            curva = self._equacao_do_flanco(angulo)
            self.flanco.append([curva['x'], curva['y']])
            angulo = angulo+incremento
            cont = cont+1
        return

    def angulo_minimo_do_flanco(self):
        """Returna o valor do angulo minimo do flanco da broca"""
        minimo = math.asin((-self.pos_A0['y'])/self.ro)
        self.angulo_min_do_flanco = minimo
        ponto_minimo = self._equacao_do_flanco(minimo)
        return

    def angulo_maximo_do_flanco(self):
        """Returna o valor do angulo maximo do flanco da broca"""
        maximo = math.asin((self.pos_B0['y'])/self.ro)
        self.angulo_max_do_flanco = maximo
        ponto_maximo = self._equacao_do_flanco(maximo)
        return

    def pos_ponta_aresta_primaria(self):
        """Returna a posicao da ponta da aresta primaria"""
        aresta = self._equacao_da_aresta_primaria(self.ro)
        hhy = self.l/math.cos(self.h) - aresta['y']
        hhx = math.sqrt((self.ro*self.ro) - (hhy*hhy))
        self.pos_B0['x'] = aresta['x']
        self.pos_B0['y'] = aresta['y']
        return

    def pos_ponta_aresta_secundaria(self):
        """Returna o valor da posicao da ponta da aresta secundaria"""
        hy = self.l/math.cos(self.h) - self.pos_B0['y']
        hx = math.sqrt((self.ro*self.ro) - (hy*hy))
        self.pos_A0['x'] = hx
        self.pos_A0['y'] = hy
        return

    def exporta_curva(
            self, file_name_normal,
            file_name_inverse, nome_da_curva):
        """Exporta as curvas do objeto broca"""
        arquivo01 = open(file_name_normal, 'w')
        arquivo02 = open(file_name_inverse, 'w')
        if(nome_da_curva == 'primaria'):
            for par in self.curva_primaria:
                arquivo01.write('%f    %f\n' % (par[0], par[1]))
                arquivo02.write('%f    %f\n' % (-par[0], -par[1]))
        if(nome_da_curva == 'secundaria'):
            for par in self.curva_secundaria:
                arquivo01.write('%f    %f\n' % (par[0], par[1]))
                arquivo02.write('%f    %f\n' % (-par[0], -par[1]))
        if(nome_da_curva == 'flanco'):
            for par in self.flanco:
                arquivo01.write('%f    %f\n' % (par[0], par[1]))
                arquivo02.write('%f    %f\n' % (-par[0], -par[1]))
        arquivo01.close()
        arquivo02.close()
        return

    def show_curva_primaria(self):
        """Mostra os pontos da curva primaria"""
        for par in self.curva_primaria:
            print(par)
        return

    def show_flanco(self):
        """Mostra os pontos da do flanco"""
        for par in self.flanco:
            print(par)
        return

    def show(self):
        """Mostra os dados basicos do objeto broca"""
        print("#######################")
        print("Diametro %f" % (self.D))
        print("Espessura da ponta da broca %f" % (self.W))
        print("Comprimento da helice %f" % (self.L))
        print("Angulo da ponta da broca %f" % (self.P))
        print("Largura transversal da helice %f" % (self.l))
        print("Metade da espessura da ponta da broca %f" % (self.t))
        print("Raio da broca %f" % (self.ro))
        print("Metade do angulo da ponta da broca %f" % (self.p))
        print("Angulo de helice %f" % (self.h))
        print("Centro em x %f" % (self.centro_x))
        print("Centro em y %f" % (self.centro_y))
        print("Posicao da aresta A0 %f, %f" %
              (self.pos_A0['x'], self.pos_A0['y']))
        print("Posicao da aresta B0 %f, %f" %
              (self.pos_B0['x'], self.pos_B0['y']))
        print("Posicao da aresta C0 %f, %f" %
              (self.pos_C0['x'], self.pos_C0['y']))
        print("Angulo minimo do flanco da broca %f" %
              (self.angulo_min_do_flanco))
        print("Angulo maximo do flanco da broca %f" %
              (self.angulo_max_do_flanco))

    def _equacao_da_aresta_primaria(self, r):
        """Returna o valor de v, x e y para a aresta primaria"""
        v = (
             math.asin(self.t/r)
             + ((math.tan(self.h)) / (math.tan(self.p)))
             * (
                (math.sqrt(r*r - self.t*self.t))
                / self.ro
               )
        )
        x = r*math.cos(v)
        y = r*math.sin(v)
        return {'x': x, 'y': y, 'v': v}

    def _equacao_do_flanco(self, angulo):
        """Returna o valor dos flancos da broca"""
        x = self.ro*math.cos(angulo)
        y = self.ro*math.sin(angulo)
        return {'x': x, 'y': y}

    def _equacao_da_aresta_secundaria(self, angulo):
        a = self.centro_curva_secundaria_x
        b = self.centro_curva_secundaria_y
        r = self.raio_curva_secundaria
        x = r*math.cos(angulo) + a
        y = r*math.sin(angulo) + b
        return {'x': x, 'y': y}

    def _angulo_minimo_curva_secundaria(self):
        a = self.centro_curva_secundaria_x
        r = self.raio_curva_secundaria
        b = self.centro_curva_secundaria_y
        X2 = -self.pos_A0['x']
        Y2 = self.pos_A0['y']
        quadrante = self._acha_quadrante(X2,Y2,a,b)
        if (quadrante == 'Quadrante 2'):
            angulo_minimo = math.acos((X2-a)/r)
        elif(quadrante == 'Quadrante 3'):
            angulo_minimo = -math.acos((X2-a)/r)+2*math.pi
        else:
            angulo_minimo = math.acos((X2-a)/r)
        return angulo_minimo

    def _angulo_maximo_curva_secundaria(self):
        a = self.centro_curva_secundaria_x
        r = self.raio_curva_secundaria
        X0 = self.pos_C0['x']
        angulo_maximo = math.acos(-(X0-a)/r)+math.pi
        return angulo_maximo

    def _acha_quadrante(self,x,y,a,b):
        quadrante = ''
        if ((x>a)and(y>b)):
            quadrante = 'Quadrante 1'
        if ((x<a)and(y>b)):
            quadrante = 'Quadrante 2'
        if ((x<a)and(y<b)):
            quadrante = 'Quadrante 3'
        if ((x>a)and(y<b)):
            quadrante = 'Quadrante 4'
        return quadrante

    def _determina_coeficientes_curva_secundaria(self):
        x0 = self.pos_C0['x']
        y0 = self.pos_C0['y']
        x1 = self.pos_B0['x']
        y1 = self.pos_B0['y']
        x2 = -self.pos_A0['x']
        y2 = self.pos_A0['y']
        T = (y0-y1)
        Z = (-x0+x1)
        C = (-x1*(y0-y1) + y1*(x0-x1))
        K = -(
              (x2*x2) + (y2*y2) - (x0+x1)*x2 - (y0+y1)*y2 + x0*x1 + y0*y1
             ) / (
                 (x2*T)+(y2*Z)+C
                 )
        a = -(1/2)*((K*T)-x0-x1)
        b = -(1/2)*((K*Z)-y0-y1)
        A1 = -2*a
        B1 = -2*b
        r = math.sqrt(((A1*A1)/4)+((B1*B1)/4)-(x0*x1+y0*y1+(K*C)))
        self.centro_curva_secundaria_x = a
        self.centro_curva_secundaria_y = b
        self.raio_curva_secundaria = r
        return


class AchaParametrosSecundarios:
    """Classe Que encontra os parametros da curva secundaria"""

    objeto_broca = None
    """Objeto da classe broca """

    matriz_F = None
    """Matriz F original do sistema nao linear"""

    matriz_J = None
    """Matriz J derivada do sistema nao linear original"""

    pontos_de_interpolacao = None
    """Pontos pelos quais o perfil secundario deve passar"""

    a = None
    """Centro em x do perfil secundario"""

    b = None
    """Centro em y do perfil secundario"""

    r = None
    """Raio do perfil secundario"""

    tol = None

    def __init__(self, broca):
        """Inicializa o objeto"""
        self.objeto_broca = broca
        self.matriz_F = []
        self.matriz_J = []
        pontos = []
        pontos.append([broca.pos_A0['x'], broca.pos_A0['y']])
        pontos.append([broca.pos_B0['x'], broca.pos_B0['y']])
        pontos.append([broca.pos_C0['x'], broca.pos_C0['y']])
        self.pontos_de_interpolacao = pontos
        self.a = 5
        self.b = 5
        self.r = 5
        self.x_primeiro = []
        self.chute_inicial = []
        self.valor_final = []
        self.tol = 0.0000000000000001
        self.max_iteracoes = 1000
        self.iteracoes = 0
        self.matriz_teste_A = []
        self.matriz_teste_B = []

    def add_chute_inicial(self, chute):
        """Seta o chute inicial"""
        self.chute_inicial = chute[:]
        return

    def monta_matrizes(self):
        """Monta as matrizes F e J"""
        for point in self.pontos_de_interpolacao:
            X = point[0]
            Y = point[1]
            linha_F = self._linha_da_matriz_F(X, Y)
            linha_J = self._linha_da_matriz_J(X, Y)
            self.matriz_F.append(linha_F)
            self.matriz_J.append(linha_J)
        return

    def show(self):
        """Mostra os dados basicos do objeto"""
        print("#######################")
        print("Objeto da broca %s" % (self.objeto_broca))
        print("Matriz F %s" % (self.matriz_F))
        print("Matriz J %s" % (self.matriz_J))
        print("Pontos de interpolacao %s" % (self.pontos_de_interpolacao))
        print("Centro em x (a) %f" % (self.a))
        print("Centro em y (b) %f" % (self.b))
        print("Raio (r) %f" % (self.r))
        return

    def _atualiza_chute(self):
        return

    def testa_gauss_siedel(self):
        self.matriz_J = [
                               [4, -1, -1, 0, 0],
                               [-1, 4, -1, -1, 0],
                               [-1, -1, 4, -1, -1],
                               [0, -1, -1, 4, -1],
                               [0, 0, -1, -1, 4]
        ]
        self.matriz_F = [-1, 2, 6, 2, -1]
        self.x_primeiro = [0, 0, 0, 0, 0]
        self.chute_inicial = [0, 0, 0, 0, 0]
        self._gauss_seidel()
        return

    def executa_gauss_seidel(self):
        self.x_primeiro = [0, 0, 0]
        self.chute_inicial = [5, 5, 5]
        self._gauss_seidel()
        return

    def _gauss_seidel(self):
        n = len(self.matriz_J)
        k = self.max_iteracoes
        x_primeiro = self.x_primeiro[:]
        x_chute = self.chute_inicial[:]
        matriz_A = self.matriz_J[:]
        matriz_B = self.matriz_F[:]
        print(matriz_A)
        for k in range(0, k):
            norma = 0
            for i in range(0, n):
                soma = 0
                for j in range(0, i):
                    soma = soma + matriz_A[i][j] * x_primeiro[j]
                for j in range(i+1, n):
                    soma = soma + matriz_A[i][j] * x_chute[j]
                x_primeiro[i] = (matriz_B[i] - soma) / matriz_A[i][i]
                norma = norma + (x_chute[i]-x_primeiro[i])**2
            if (math.sqrt(norma) <= self.tol):
                x_chute = x_primeiro[:]
                self.valor_final = x_chute[:]
                break
            x_chute = x_primeiro[:]
            print(x_chute)
        return

    def _erro(self, vetor_1, vetor_2):
        n = len(vetor_1)
        vetor_resposta = []
        for item in range(0, n):
            vetor_resposta.append(abs(vetor_1[item] - vetor_2[item]))
        return vetor_resposta

    def _linha_da_matriz_F(self, X, Y):
        """Seta uma linha da matriz F"""
        a = self.a
        b = self.b
        r = self.r
        linha = a*a + b*b - r*r - 2*a*X - 2*b*Y + X*X + Y*Y
        return linha

    def _linha_da_matriz_J(self, X, Y):
        """Seta uma linha da matriz J"""
        a = self.a
        b = self.b
        r = self.r
        linha = [2*a - 2*X, 2*b - 2*Y, -2*r]
        return linha
