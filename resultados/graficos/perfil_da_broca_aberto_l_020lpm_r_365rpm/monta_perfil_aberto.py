#!/usr/bin/python

from primitives import FatiaDoPerfil, Ponto, Perfil

lista_de_parametros = ['"Points:0"','"Points:1"','"Points:2"',
                       '"Temperatura (C)"'
]

lista_de_arquivos = [ "secao_000_mm0.csv", "secao_005_mm0.csv",
                      "secao_010_mm0.csv","secao_015_mm0.csv",
                      "secao_020_mm0.csv","secao_025_mm0.csv",
                      "secao_030_mm0.csv","secao_035_mm0.csv",
                      "secao_040_mm0.csv","secao_045_mm0.csv",
                      "secao_050_mm0.csv","secao_055_mm0.csv",
                      "secao_060_mm0.csv","secao_065_mm0.csv",
                      "secao_070_mm0.csv","secao_075_mm0.csv",
                      "secao_080_mm0.csv","secao_085_mm0.csv",
                      "secao_090_mm0.csv","secao_095_mm0.csv",
                      "secao_0100_mm0.csv"
]

arquivo_de_saida = "perfil_aberto_da_broca.dat"

perfil_completo = Perfil(lista_de_parametros,lista_de_arquivos)
perfil_completo.processa_arquivos()
perfil_completo.acha_maximo_numero_de_pontos()
perfil_completo.show()
perfil_completo.exporta_arquivos(arquivo_de_saida, 350)

#for arquivo in lista_de_arquivos:
#    nome_do_arquivo = arquivo # "secao_000_mm0.csv"
#    perfil = FatiaDoPerfil(nome_do_arquivo)
#    perfil.add_lista_de_parametros(lista_de_parametros)
#    perfil.carrega_arquivo()
#    perfil.processa_arquivo()
#    perfil.organiza_pontos()
#    perfil.converte_angulo_em_radianos()
#    perfil.show_points()
