#!/usr/bin/python

import math
from operator import attrgetter


class Perfil:
    """Classe Perfil"""

    lista_de_parametros = None
    """Nome dos parametros a serem lidos nos arquivos de saida do Paraview"""

    lista_de_arquivos = None
    """Nome dos arquivos a serem lidos"""

    lista_de_fatias = None
    """Lista de fatias do perfil"""

    numero_maximo_de_pontos = None
    """Numero maximo de pontos de cada fatia do perfil"""

    lista_de_fatias_interpoladas = None
    """Lista de fatias interpoladas"""

    def __init__(self, lista_de_parametros, lista_de_arquivos):
        """Inicializa o objeto"""
        self.lista_de_parametros = lista_de_parametros
        self.lista_de_arquivos = lista_de_arquivos
        self.lista_de_fatias = []
        self.numero_maximo_de_pontos = 0
        self.lista_de_fatias_interpoladas = []

    def processa_arquivos(self):
        """Processa todos os arquivos do perfil"""
        for arquivo in self.lista_de_arquivos:
            fatia_do_perfil = FatiaDoPerfil(arquivo)
            fatia_do_perfil.add_lista_de_parametros(self.lista_de_parametros)
            fatia_do_perfil.carrega_arquivo()
            fatia_do_perfil.processa_arquivo()
            fatia_do_perfil.organiza_pontos()
            fatia_do_perfil.converte_angulo_em_radianos()
            fatia_do_perfil.acha_numero_de_pontos()
            self.lista_de_fatias.append(fatia_do_perfil)
            # fatia_do_perfil.show_points()
        return

    def processa_arquivos_com_interpolacao(self):
        """Processa todos os arquivos com interpolacao do perfil"""
        for arquivo in self.lista_de_arquivos:
            perfil = FatiaDoPerfil(arquivo)
            perfil.add_lista_de_parametros(self.lista_de_parametros)
            perfil.carrega_arquivo()
            perfil.processa_arquivo()
            perfil_interpolado = FatiaDoPerfilInterpolado()
            perfil_interpolado.add_lista_de_pontos_de_interpolacao(perfil.lista_de_pontos)
            perfil_interpolado.add_tipo_de_interpolacao('linear')
            perfil_interpolado.add_inicio_da_curva_interpolada(0)
            perfil_interpolado.add_fim_da_curva_interpolada(0.068806)
            perfil_interpolado.add_quantidade_de_pontos_da_curva(200)
            perfil_interpolado.gera_lista_de_pontos_interpolados()
            perfil_interpolado.processa_pontos()
            perfil_interpolado.executa_interpolacao()
            self.lista_de_fatias_interpoladas.append(perfil_interpolado)
        return

    def exporta_arquivos_interpolados(self, nome_do_arquivo):
        """Exporta o perfil interpolado para o formato que o latex leia"""
        arquivo = open(nome_do_arquivo, 'w')
        for fatia in self.lista_de_fatias_interpoladas:
            cont = 0
            for ponto in fatia.lista_de_pontos_interpolados:
                str_coord_y = str(ponto.posicao_do_plano)
                str_comp_arco = str(ponto.comprimento_de_arco)
                str_temperatura = str(ponto.temperatura)
                line = str_coord_y + " " + str_comp_arco + " " + str_temperatura
                arquivo.write(line + "\n")
                cont = cont + 1
            arquivo.write("\n")
        arquivo.close()
        return

    def exporta_arquivos(self, nome_do_arquivo, maximo):
        """Exporta o perfil para o formato que o latex leia"""
        arquivo = open(nome_do_arquivo, 'w')
        for fatia in self.lista_de_fatias:
            cont = 0
            for cont in range(0, maximo):
                ponto = fatia.lista_de_pontos[cont]
                str_coord_y = str(ponto.coord_y)
                str_angulo = str(ponto.angulo_do_ponto)
                str_temperatura = str(ponto.temperatura)
                line = str_coord_y + " " + str_angulo + " " + str_temperatura
                arquivo.write(line + "\n")
                cont = cont + 1
            arquivo.write("\n")
        arquivo.close()
        return

    def acha_maximo_numero_de_pontos(self):
        """Acha maximo numero de pontos que podem ser exportados"""
        ponto_min = min(self.lista_de_fatias, key=attrgetter('numero_de_pontos'
                                                             )
                        )
        self.numero_maximo_de_pontos = ponto_min.numero_de_pontos

    def show_original(self):
        """Mostra os atributos do perfil"""
        print("A lista de parametros lidos e:")
        for parametro in self.lista_de_parametros:
            print(parametro)
        print("A lista de arquivos lidos e:")
        for arquivo in self.lista_de_arquivos:
            print(arquivo)
        print("A lista de fatias do perfil e:")
        for fatia in self.lista_de_fatias:
            print(fatia)
        print("O numero de pontos de cada fatia e:")
        for fatia in self.lista_de_fatias:
            print(len(fatia.lista_de_pontos))
        print("O maximo numero de pontos e:")
        print(self.numero_maximo_de_pontos)
        return

    def show_interpolados(self):
        """Mostra os atributos do perfil"""
        print("A lista de parametros lidos e:")
        for parametro in self.lista_de_parametros:
            print(parametro)
        print("A lista de arquivos lidos e:")
        for arquivo in self.lista_de_arquivos:
            print(arquivo)
        print("A lista de fatias do perfil e:")
        for fatia in self.lista_de_fatias_interpoladas:
            print(fatia)
        print("O numero de pontos de cada fatia e:")
        for fatia in self.lista_de_fatias_interpoladas:
            print(len(fatia.lista_de_pontos_interpolados))
        return


class FatiaDoPerfil:
    """Classe Fatia do Perfil"""

    nome_do_arquivo = None
    """Nome do arquivo do perfil"""

    lista_de_parametros = None
    """Lista de parametros do perfil"""

    lista_de_linhas_do_arquivo = None
    """Lista de linhas do arquivo"""

    primeira_linha_do_arquivo = None
    """Primeira linha do arquivo"""

    local_dos_parametros = None
    """Local dos parametros"""

    lista_de_pontos = None
    """Lista de pontos do perfil"""

    numero_de_pontos = None
    """Numero de pontos do perfil"""

    def __init__(self, nome_do_arquivo):
        """Inicializa o objeto"""
        self.nome_do_arquivo = nome_do_arquivo
        self.lista_de_parametros = []
        self.lista_de_linhas_do_arquivo = []
        self.primeira_linha_do_arquivo = ""
        self.local_dos_parametros = []
        self.lista_de_pontos = []
        self.numero_de_pontos = 0

    def add_lista_de_parametros(self, lista_de_parametros):
        """Adiciona lista de parametros"""
        self.lista_de_parametros = lista_de_parametros
        return

    def carrega_arquivo(self):
        """Carrega todos as linhas do arquivo em uma lista"""
        arquivo = open(self.nome_do_arquivo, 'r')
        for linha in arquivo:
            sub_linha = linha.split('\n')
            linha_desmontada = sub_linha[0].split(',')
            self.lista_de_linhas_do_arquivo.append(linha_desmontada)
        arquivo.close()
        self.primeira_linha_do_arquivo = self.lista_de_linhas_do_arquivo[0]
        self.lista_de_linhas_do_arquivo.remove(self.primeira_linha_do_arquivo)
        return

    def processa_arquivo(self):
        """Transforma a lista de linhas em objetos da classe Pontos
        pertencentes ao perfil de temperaturas"""
        self._encontra_local_dos_parametros()
        local_x = self.local_dos_parametros[0]
        local_y = self.local_dos_parametros[1]
        local_z = self.local_dos_parametros[2]
        local_temperatura = self.local_dos_parametros[3]
        local_comprimento_de_arco = self.local_dos_parametros[4]
        for elemento in self.lista_de_linhas_do_arquivo:
            coord_x = elemento[local_x]
            coord_y = elemento[local_y]
            coord_z = elemento[local_z]
            temperatura = elemento[local_temperatura]
            comprimento = elemento[local_comprimento_de_arco]
            temp_ponto = Ponto()
            temp_ponto.add_coord_x(float(coord_x))
            temp_ponto.add_coord_y(float(coord_y))
            temp_ponto.add_coord_z(float(coord_z))
            temp_ponto.add_temperatura(float(temperatura))
            temp_ponto.add_comprimento_de_arco(float(comprimento))
            temp_ponto.calcula_angulo_e_quadrante()
            self.lista_de_pontos.append(temp_ponto)
        return

    def organiza_pontos(self):
        """Organiza a lista de ponto em ordem crescente de angulo"""
        self.lista_de_pontos.sort(key=lambda ponto: ponto.angulo_do_ponto,
                                  reverse=False
                                  )
        return

    def organiza_pontos_melhorado(self):
        """Organiza a lista de ponto em ordem crescente de angulo"""
        for element in self.lista_de_pontos:
            if (self.lista_de_pontos[-1].quadrante ==
               self.lista_de_pontos[0].quadrante):
                temp_element = self.lista_de_pontos[-1]
                self.lista_de_pontos.pop()
                self.lista_de_pontos.insert(0, temp_element)
        return

    def acha_numero_de_pontos(self):
        """Acha o numero de pontos do perfil"""
        self.numero_de_pontos = len(self.lista_de_pontos)
        return

    def converte_angulo_em_radianos(self):
        """Converte de graus para radianos o angulo de cada ponto"""
        for ponto in self.lista_de_pontos:
            ponto.angulo_do_ponto = math.radians(ponto.angulo_do_ponto)
        return

    def exporta_fatia_do_perfil(self, nome_do_arquivo):
        """Exporta fatia do perfil para o formato que o latex leia"""
        arquivo = open(nome_do_arquivo, 'w')
        for ponto in self.lista_de_pontos:
            str_coord_y = str(ponto.coord_y)
            str_angulo = str(ponto.angulo_do_ponto)
            str_temperatura = str(ponto.temperatura)
            line = str_coord_y + " " + str_angulo + " " + str_temperatura
            arquivo.write(line + "\n")
        arquivo.close()
        return

    def exporta_fatia_do_perfil_em_arco(self, nome_do_arquivo):
        """Exporta fatia do perfil para o formato que o latex leia"""
        arquivo = open(nome_do_arquivo, 'w')
        for ponto in self.lista_de_pontos:
            str_coord_y = str(ponto.coord_y)
            str_arco = str(ponto.comprimento_de_arco)
            str_temperatura = str(ponto.temperatura)
            line = str_coord_y + " " + str_arco + " " + str_temperatura
            arquivo.write(line + "\n")
        arquivo.close()
        return

    def _encontra_local_dos_parametros(self):
        """Encontra local dos parametros na linha"""
        for parametro in self.lista_de_parametros:
            local = 0
            cont = 0
            for elemento in self.primeira_linha_do_arquivo:
                if(elemento == parametro):
                    local = cont
                cont = cont + 1
            self.local_dos_parametros.append(local)
        return

    def show_points(self):
        """Mostra os dados de cada ponto da fatia do perfil"""
        print("As coordenadas, o valor da temperatura, o angulo e quadrante"
              " sao:"
              )
        for objeto in self.lista_de_pontos:
            print("Coordenada x: %s y: %s z: %s , temperatura: %s , angulo: %f"
                  " , quadrante: %s e comprimento de arco de %s"
                  % (objeto.coord_x, objeto.coord_y, objeto.coord_z,
                     objeto.temperatura, objeto.angulo_do_ponto,
                     objeto.quadrante, objeto.comprimento_de_arco
                     )
                  )
        return

    def show(self):
        """Mostra conteudo do objeto"""
        print("Nome do arquivo: %s" % (self.nome_do_arquivo))
        print("Lista de parametros lidos do arquivo: %s"
              % (self.lista_de_parametros)
              )
        print("A primeira linha do arquivo e:")
        print(self.primeira_linha_do_arquivo)
        print ("Linhas do arquivo")
        for linha in self.lista_de_linhas_do_arquivo:
            print(linha)
        print("O local dos parametros e: %s" % (self.local_dos_parametros))
        print("Os objetos da classe Ponto pertencentes a esse objeto sao:")
        for objeto in self.lista_de_pontos:
            print(objeto)
        print("As coordenadas x, y, z e o valor do angulo e da temperatura"
              " sao:"
              )
        for objeto in self.lista_de_pontos:
            print("Coordenada x: %s y: %s z: %s com temperatura de %s e com "
                  "angulo de %f e quadrante de %s e comprimento de arco de %f"
                  % (objeto.coord_x, objeto.coord_y, objeto.coord_z,
                     objeto.temperatura, objeto.angulo_do_ponto,
                     objeto.quadrante, objeto.comprimento_de_arco
                     )
                  )
        return


class Ponto:
    """Classe Ponto"""

    coord_x = None
    """Coordenada x do ponto"""

    coord_y = None
    """Coordenada y do ponto"""

    coord_z = None
    """Coordenada z do ponto"""

    temperatura = None
    """Temperatura do ponto"""

    angulo_do_ponto = None
    """Angulo do ponto"""

    quadrante = None
    """quadrante do ponto"""

    comprimento_de_arco = None
    """comprimento de arco"""

    def __init__(self):
        """Inicializa o objeto"""
        self.coord_x = 0
        self.coord_y = 0
        self.coord_z = 0
        self.temperatura = 0
        self.angulo_do_ponto = 0
        self.quadrante = 0
        self.comprimento_de_arco = 0

    def get_class_name(self):
        """Pega o nome da classe"""
        return self.__class__.__name__

    def add_coord_x(self, coord_x):
        """Adiciona coordenada x do ponto"""
        self.coord_x = coord_x
        return

    def add_coord_y(self, coord_y):
        """Adiciona coordenada y do ponto"""
        self.coord_y = coord_y
        return

    def add_coord_z(self, coord_z):
        """Adiciona coordenada z do ponto"""
        self.coord_z = coord_z
        return

    def add_temperatura(self, temperatura):
        """Adiciona temperatura do ponto"""
        self.temperatura = temperatura
        return

    def add_comprimento_de_arco(self, comprimento):
        """Adiciona comprimento de arco"""
        self.comprimento_de_arco = comprimento
        return

    def calcula_angulo_e_quadrante(self):
        """Calcula angulo e quadrante do ponto"""
        x = float(self.coord_x)
        z = float(self.coord_z)
        if ((x >= 0)and(z >= 0)):
            angulo = math.degrees(math.atan(x/z))
            self.angulo_do_ponto = angulo
            self.quadrante = 1
        elif((x >= 0)and(z < 0)):
            angulo = math.degrees(math.atan(x/z))
            self.angulo_do_ponto = angulo + 180
            self.quadrante = 2
        elif ((x < 0)and(z < 0)):
            angulo = math.degrees(math.atan(x/z))
            self.angulo_do_ponto = angulo + 180
            self.quadrante = 3
        else:
            angulo = math.degrees(math.atan(x/z))
            self.angulo_do_ponto = angulo + 360
            self.quadrante = 4
        return

    def show(self):
        """Mostra conteudo do ponto"""
        print("A coordenada x desse ponto e: %s" % (self.coord_x))
        print("A coordenada y desse ponto e: %s" % (self.coord_y))
        print("A coordenada z desse ponto e: %s" % (self.coord_z))
        print("A temperatura desse ponto e: %s" % (self.temperatura))
        print("O angulo desse ponto e: %s" % (self.angulo_do_ponto))
        print("O quadrante desse ponto e: %s" % (self.quadrante))
        return


class FatiaDoPerfilInterpolado:
    """Classe Fatia do Perfil Interpolado"""

    lista_de_interpolacao = None
    """Lista de pontos de interpolacao do perfil"""

    tipo_de_interpolacao = None
    """Tipo de interpolacao da curva do perfil"""

    inicio_da_curva_interpolada = None
    """Inicio da interpolacao da curva do perfil"""

    fim_da_curva_interpolada = None
    """Fim da interpolacao da curva do perfil"""

    quantidade_de_pontos_da_curva = None
    """Quantidade de pontos da interpolacao da curva do perfil"""

    incremento_angular = None
    """Incremento angular da curva do perfil"""

    lista_de_pontos_interpolados = None
    """Lista de pontos interpolados da curva"""

    lista_de_pontos_misturados = None
    """Lista de pontos interpolados e originais da curva"""

    def __init__(self):
        """Inicializa o objeto"""
        self.lista_de_interpolacao = []
        self.tipo_de_interpolacao = ''
        self.inicio_da_curva_interpolada = 0
        self.fim_da_curva_interpolada = 0
        self.quantidade_de_pontos_da_curva = 0
        self.incremento_de_arco = 0
        self.lista_de_pontos_interpolados = []
        self.lista_de_pontos_misturados = []

    def add_lista_de_pontos_de_interpolacao(self, lista_de_pontos):
        """Adiciona lista de pontos de interpolacao"""
        self.lista_de_interpolacao = lista_de_pontos
        return

    def add_tipo_de_interpolacao(self, tipo_de_interpolacao):
        """Adiciona tipo de interpolacao"""
        self.tipo_de_interpolacao = tipo_de_interpolacao
        return

    def add_inicio_da_curva_interpolada(self, inicio_da_curva_interpolada):
        """Adiciona inicio da curva interpolada"""
        self.inicio_da_curva_interpolada = inicio_da_curva_interpolada
        return

    def add_fim_da_curva_interpolada(self, fim_da_curva_interpolada):
        """Adiciona fim da curva interpolada"""
        self.fim_da_curva_interpolada = fim_da_curva_interpolada
        return

    def add_quantidade_de_pontos_da_curva(self, quantidade_de_pontos_da_curva):
        """Adiciona fim da curva interpolada"""
        self.quantidade_de_pontos_da_curva = quantidade_de_pontos_da_curva - 2
        return

    def gera_lista_de_pontos_interpolados(self):
        """Gera lista de pontos interpolados da curva interpolada"""
        self._calcula_incremento()
        arco = self.inicio_da_curva_interpolada
        for cont in range(0, self.quantidade_de_pontos_da_curva + 2):
            ponto_interpolado = Interpoladores()
            ponto_interpolado.add_tipo(self.tipo_de_interpolacao)
            ponto_interpolado.add_comprimento_de_arco(arco)
            self.lista_de_pontos_interpolados.append(ponto_interpolado)
            arco = arco + self.incremento_de_arco
        return

    def processa_pontos(self):
        """Processa os pontos interpolados da curva"""
        self._gera_lista_misturada()
        self._ordena_lista_misturada()
        # self.lista_de_pontos_misturados.pop(0)  # Obs: Tirar essa linha
        self._encontra_pontos_originais_vizinhos()
        return

    def _gera_lista_misturada(self):
        """Gera lista com pontos originais da curva e com pontos a serem
        interpolados"""
        self.lista_de_pontos_misturados = (self.lista_de_interpolacao +
                                           self.lista_de_pontos_interpolados)
        return

    def _ordena_lista_misturada(self):
        """Ordena lista com pontos originais da curva e com pontos a serem
        interpolados por ordem de grandeza do comprimento do arco"""
        self.lista_de_pontos_misturados.sort(key=lambda ponto:
                                             ponto.comprimento_de_arco,
                                             reverse=False
                                             )
        return

    def _encontra_pontos_originais_vizinhos(self):
        """Dentro da lista misturada e ordenada encontra os pontos originais
        vizinhos dos pontos a serem interpolados"""
        contador = 0
        lista_mist = self.lista_de_pontos_misturados
        for ponto in self.lista_de_pontos_misturados:
            if(isinstance(ponto, Interpoladores)):
                if(contador == 0):
                    ponto_vizinho_depois = lista_mist[contador + 1]
                    ponto.add_vizinho_posterior(ponto_vizinho_depois)
                if(contador == len(self.lista_de_pontos_misturados) - 1):
                    ponto_vizinho_antes = lista_mist[contador - 1]
                    ponto.add_vizinho_anterior(ponto_vizinho_antes)
                if((contador != 0)and(contador !=
                                      len(self.lista_de_pontos_misturados) - 1
                                      )
                   ):
                    vizinho_antes = self._acha_vizinho(contador, "anterior")
                    ponto_vizinho_antes = lista_mist[vizinho_antes]
                    ponto.add_vizinho_anterior(ponto_vizinho_antes)
                    vizinho_depois = self._acha_vizinho(contador, "posterior")
                    ponto_vizinho_depois = lista_mist[vizinho_depois]
                    ponto.add_vizinho_posterior(ponto_vizinho_depois)
            contador = contador + 1
        return

    def _acha_vizinho(self, contador, direcao):
        """Encontra ponto original mais proximo anterior e posterior"""
        if (direcao == "anterior"):
            incremento = -1
        else:
            incremento = 1
        cont_temp = contador
        lista = self.lista_de_pontos_misturados
        while((cont_temp >= 0)and(cont_temp <= len(lista))):
            cont_temp = cont_temp + incremento
            item = lista[cont_temp]
            if(isinstance(item, Ponto)):
                break
        return cont_temp

    def _substitui_vizinho_por_respectivo_objeto(self):
        """Substitui a posicao do vizinho pelo respectivo objeto"""
        lista_misturada = self.lista_de_pontos_misturados
        for ponto in self.lista_de_pontos_interpolados:
            vizinho_posterior = lista_misturada[ponto.vizinho_posterior]
            ponto.add_vizinho_posterior(lista_misturada)
        return

    def executa_interpolacao(self):
        """Executa interpolacao"""
        for ponto in self.lista_de_pontos_interpolados:
            ponto.executa_interpolacao_linear()
        return

    def exporta_fatia_do_perfil_em_arco(self, nome_do_arquivo):
        """Exporta fatia do perfil interpolado para o formato que o latex leia"""
        arquivo = open(nome_do_arquivo, 'w')
        for ponto in self.lista_de_pontos_interpolados:
            str_coord_y = str(ponto.posicao_do_plano)
            str_arco = str(ponto.comprimento_de_arco)
            str_temperatura = str(ponto.temperatura)
            line = str_coord_y + " " + str_arco + " " + str_temperatura
            arquivo.write(line + "\n")
        arquivo.close()
        return

    def show(self):
        """Mostra as propriedades dos pontos interpolados"""
        for ponto in self.lista_de_pontos_interpolados:
            ponto.show()
        return

    def show_completo(self):
        """Mostra o conteudo da fatia do perfil interpolado"""
        print("A lista de pontos possui %s pontos"
              % (len(self.lista_de_interpolacao))
              )
        print("Tipo de interpolacao: %s "
              % (self.tipo_de_interpolacao)
              )
        print("Inicio da curva interpolada: %s "
              % (self.inicio_da_curva_interpolada)
              )
        print("Fim da curva interpolada: %s "
              % (self.fim_da_curva_interpolada)
              )
        print("A quantidade de pontos da curva interpolada e: %s "
              % (self.quantidade_de_pontos_da_curva)
              )
        print("O incremento de arco da curva interpolada e: %f "
              % (self.incremento_de_arco)
              )
        print("A lista de pontos interpolados possui %s pontos"
              % (len(self.lista_de_pontos_interpolados))
              )
        print("O valor de arco e da posicao na lista misturada:")
        contador = 0
        for item in self.lista_de_pontos_misturados:
            if (isinstance(item, Interpoladores)):
                anterior = item.vizinho_anterior
                if (anterior != -1):
                    anterior = anterior.comprimento_de_arco
            else:
                anterior = "NA"
            if (isinstance(item, Interpoladores)):
                posterior = item.vizinho_posterior
                if (posterior != -1):
                    posterior = posterior.comprimento_de_arco
            else:
                posterior = "NA"
            print("Arco: %f, numero: %i, classe: %s, ponto interpolador: %s"
                  " antes: %s, depois: %s"
                  % (item.comprimento_de_arco, contador, item.get_class_name(),
                     isinstance(item, Interpoladores), anterior, posterior
                     )
                  )
            contador = contador + 1
        return

    def _calcula_incremento(self):
        """Calcula o valor do incremento dos pontos interpolados"""
        self.incremento_de_arco = (self.fim_da_curva_interpolada -
                                   self.inicio_da_curva_interpolada
                                   ) / (self.quantidade_de_pontos_da_curva + 1)
        return


class Interpoladores:
    """Classe de Interpoladores"""

    tipo_de_interpolacao = None
    """Tipo de interpolacao entre os pontos vizinhos"""

    posicao_do_plano = None
    """Posicao do plano do ponto"""

    comprimento_de_arco = None
    """Valor do comprimento de arco do ponto interpolado"""

    temperatura = None
    """Valor da temperatura do ponto"""

    lista_de_vizinhos_anteriores = None
    """Lista de vizinhos anteriores"""

    lista_de_vizinhos_posteriores = None
    """Lista de vizinhos posteriores"""

    vizinho_anterior = None
    """Ponto vizinho anterior"""

    vizinho_posterior = None
    """Ponto vizinho posterior"""

    def __init__(self):
        """Inicializa o objeto"""
        self.tipo_de_interpolacao = ' '
        self.posicao_do_plano = 0
        self.comprimento_de_arco = 0
        self.temperatura = 0
        self.vizinho_anterior = -1
        self.vizinho_posterior = -1
        self.lista_de_vizinhos_anteriores = []
        self.lista_de_vizinhos_posteriores = []

    def get_class_name(self):
        """Pega o nome da classe"""
        return self.__class__.__name__

    def add_tipo(self, tipo):
        """Adiciona tipo de interpolacao"""
        self.tipo_de_interpolacao = tipo
        return

    def _add_posicao_do_plano(self, posicao):
        """Adiciona posicao do plano do ponto"""
        self.posicao_do_plano = posicao

    def add_comprimento_de_arco(self, comprimento_de_arco):
        """Adiciona o comprimento de arco do ponto"""
        self.comprimento_de_arco = comprimento_de_arco

    def add_vizinho_posterior(self, vizinho_posterior):
        """Adiciona posicao ponto vizinho posterior na lista misturada"""
        self.vizinho_posterior = vizinho_posterior

    def add_vizinho_anterior(self, vizinho_anterior):
        """Adiciona posicao ponto vizinho anterior na lista misturada"""
        self.vizinho_anterior = vizinho_anterior

    def executa_interpolacao_linear(self):
        """Executa a interpolacao do ponto com seus vizinhos"""
        if (self.vizinho_anterior == -1):
            temperatura = self.vizinho_posterior.temperatura
            pos_plano = self.vizinho_posterior.coord_y
            self.temperatura = temperatura
            self.posicao_do_plano = pos_plano
        elif (self.vizinho_posterior == -1):
            temperatura = self.vizinho_anterior.temperatura
            pos_plano = self.vizinho_anterior.coord_y
            self.temperatura = temperatura
            self.posicao_do_plano = pos_plano
        else:
            x = self.comprimento_de_arco
            pos_plano = self.vizinho_anterior.coord_y
            self.posicao_do_plano = pos_plano
            x1 =self.vizinho_anterior.comprimento_de_arco
            x2 =self.vizinho_posterior.comprimento_de_arco
            y1 =self.vizinho_anterior.temperatura
            y2 =self.vizinho_posterior.temperatura
            a = (y1 - y2)/(x1 - x2)
            b = y2 - a * x2
            temperatura = a * x + b
            self.temperatura = temperatura
        return

    def show(self):
        """Mostra propriedades do ponto"""
        print(("Tipo: %s, plano: %f, arco: %f, temperatura: %f")
               % (self.tipo_de_interpolacao, self.posicao_do_plano, self.comprimento_de_arco, self.temperatura))
        return
