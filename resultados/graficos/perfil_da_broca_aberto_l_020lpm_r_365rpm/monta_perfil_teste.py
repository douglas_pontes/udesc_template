#!/usr/bin/python

from primitives import FatiaDoPerfil, Ponto, Perfil, FatiaDoPerfilInterpolado
import math

lista_de_parametros = ['"Points:0"','"Points:1"','"Points:2"',
                       '"Temperatura (C)"','"arc_length"'
]

lista_de_arquivos = [ "secao_000_mm0.csv", "secao_005_mm0.csv",
                      "secao_010_mm0.csv","secao_015_mm0.csv",
                      "secao_020_mm0.csv","secao_025_mm0.csv",
                      "secao_030_mm0.csv","secao_035_mm0.csv",
                      "secao_040_mm0.csv","secao_045_mm0.csv",
                      "secao_050_mm0.csv","secao_055_mm0.csv",
                      "secao_060_mm0.csv","secao_065_mm0.csv",
                      "secao_070_mm0.csv","secao_075_mm0.csv",
                      "secao_080_mm0.csv","secao_085_mm0.csv",
                      "secao_090_mm0.csv","secao_095_mm0.csv",
                      "secao_0100_mm0.csv"
]

arquivo_de_saida = "perfil_aberto_da_broca.dat"

perfil_completo = Perfil(lista_de_parametros,lista_de_arquivos)
perfil_completo.processa_arquivos_com_interpolacao()
#perfil_completo.acha_maximo_numero_de_pontos()
perfil_completo.show_interpolados()
perfil_completo.exporta_arquivos_interpolados(arquivo_de_saida)
#perfil_completo.exporta_arquivos(arquivo_de_saida, 350)

#for arquivo in lista_de_arquivos:
#nome_do_arquivo = "secao_000_mm0_teste.csv"
"""
nome_do_arquivo = lista_de_arquivos[0]
perfil = FatiaDoPerfil(nome_do_arquivo)
perfil.add_lista_de_parametros(lista_de_parametros)
perfil.carrega_arquivo()
perfil.processa_arquivo()
perfil.show_points()
perfil_interpolado = FatiaDoPerfilInterpolado()
perfil_interpolado.add_lista_de_pontos_de_interpolacao(perfil.lista_de_pontos)
perfil_interpolado.add_tipo_de_interpolacao('linear')
perfil_interpolado.add_inicio_da_curva_interpolada(0)
perfil_interpolado.add_fim_da_curva_interpolada(0.068806)
perfil_interpolado.add_quantidade_de_pontos_da_curva(350)
perfil_interpolado.gera_lista_de_pontos_interpolados()
perfil_interpolado.processa_pontos()
#perfil_interpolado.add_pontos_vizinhos()
#perfil_interpolado.executa_interpolacao()
#perfil_interpolado.show_completo()
perfil_interpolado.executa_interpolacao()
perfil_interpolado.show()



perfil.exporta_fatia_do_perfil_em_arco("perfil_plano1.dat")
perfil_interpolado.exporta_fatia_do_perfil_em_arco("perfil_plano2.dat")
#perfil.exporta_fatia_do_perfil("perfil_plano1.dat")
#perfil.organiza_pontos()
#    perfil.converte_angulo_em_radianos()
"""
