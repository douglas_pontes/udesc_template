\select@language {portuguese}
\contentsline {chapter}{Lista de Figuras}{xix}
\contentsline {chapter}{Lista de Tabelas}{xxi}
\contentsline {chapter}{Lista de S\IeC {\'\i }mbolos}{xxiii}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{1}
\contentsline {chapter}{\numberline {2}Estado da Arte}{3}
\contentsline {chapter}{\numberline {3}Embasamento Te\IeC {\'o}rico}{5}
\contentsline {chapter}{\numberline {4}Aparato experimental}{7}
\contentsline {chapter}{\numberline {5}Modelamento num\IeC {\'e}rico}{9}
\contentsline {chapter}{\numberline {6}Resultados e discuss\IeC {\~o}es}{11}
\contentsline {subsection}{\numberline {6.0.1}Exemplo de gr\IeC {\'a}fico bipartido}{11}
\contentsline {subsection}{\numberline {6.0.2}Exemplo de gr\IeC {\'a}fico de barras com n\IeC {\'u}mero de colunas diferentes}{12}
\contentsline {section}{\numberline {6.1}Exemplo de gr\IeC {\'a}fico 3D com execu\IeC {\c c}\IeC {\~a}o externa}{13}
\contentsline {subsection}{\numberline {6.1.1}Exemplo de gr\IeC {\'a}fico de barras com n\IeC {\'u}mero de colunas diferentes e linha m\IeC {\'e}dia}{14}
\contentsline {subsection}{\numberline {6.1.2}Exemplo de gr\IeC {\'a}fico que pega dados de um arquivo e plota uma curva segundo uma equa\IeC {\c c}\IeC {\~a}o simultaneamente.}{15}
\contentsline {chapter}{\numberline {7}Conclus\IeC {\~o}es}{17}
\contentsline {chapter}{\numberline {8}Propostas para trabalhos futuros}{19}
\contentsline {chapter}{Ap\IeC {\^e}ndices}{19}
\contentsline {chapter}{\numberline {A}Tabelas}{21}
\contentsline {chapter}{\numberline {B}An\IeC {\'a}lise Dimensional}{23}
\contentsline {chapter}{Bibliografia}{25}
