\usepackage{xargs}
\usepackage[intoc]{nomencl} % Permite a geração da nomenclatura
\usepackage{etoolbox}
\usepackage{xcolor}


\ProvidesPackage{document_class_includes/udesc_mestrado_comandos}
\newif\if@myfiledraft
\DeclareOption{draft}{\@myfiledrafttrue}

\ProcessOptions\relax

% Define a cor vermelha
\definecolor{vermelho@}{RGB}{255,0,0}%

% Cria documento auxiliar
\newwrite\arquivoDeSimbolos%

% Cria explicação padrão
\def\explicacaoPadrao{\textcolor{vermelho@}{Este s\'imbolo est\'a sem %
                      explica\c{c}\~ao}}%

% Cria nome do arquivo auxiliar de símbolos
\def\arquivoAuxiliar{simbolos_mestrado_UDESC.uax}%

%Cria explicação padrão
\def\simboloIndefinido{\textcolor{vermelho@}{INDEFINIDO}}%

% Comando que adiciona grupo ao arquivo
\newcommand{\addGrupoDeNom}[2]{%
  \expandafter\newcommand\csname grupoNome#2\endcsname{#1}%
  \executaOsGrupos%
}

% Comando que executa os grupos
\newcommand{\executaOsGrupos}{%
  \protect%
  \renewcommand\nomgroup[1]{%
    \item[\bfseries%
          \expandafter\@nameuse{grupoNome##1}%
    ]%
  }%
}%

% Comando importante: redefine o comando da nomenclatura!!%%%%%%
%% Esse comando adiciona sempre o mesmo nomeqref para todos os símbolos fazendo
%% com que os repetidos em seções diferentes não sejam repetidos lista de
%% símbolos
\def\@@@nomenclature[#1]#2#3{%
 \def\@tempa{#2}\def\@tempb{#3}%
 \protected@write\@nomenclaturefile{}%
  {\string\nomenclatureentry{#1\nom@verb\@tempa @[{\nom@verb\@tempa}]%
   \begingroup\nom@verb\@tempb\protect\nomeqref{1.1.1}|nompageref}{1}}%
 \endgroup%
 \@esphack}%

% Expande se for comando
\newcommand{\expandeSeComando}[1]{\ifcsdef{#1}{\expandafter\csname #1\endcsname}{#1}}

% Comando que define o simbolo
\newcommandx{\defineSimbolo}[6][1=A,2=A,3=01,6=null]{\expandafter\newcommand\csname%
                               grupo#4\endcsname{#1}%
                               \expandafter\newcommand\csname%
                               letra#4\endcsname{#2}%
                               \expandafter\newcommand\csname%
                               ordem#4\endcsname{#3}%
                               \expandafter\newcommand\csname%
                               simbolo#4\endcsname{\text{$#5$}}%
                               \expandafter\newcommand\csname%
                               simboloNeg#4\endcsname{\text{$\boldsymbol{#5}$}}%
                               \expandafter\newcommand\csname%
                               unidade#4\endcsname{#6}%
                               \comandoDoSimbolo{#4}%
                               }%

% Comando que define explicacao do simbolo
\newcommand{\defineExplicacao}[2]{\expandafter\newcommand\csname%
                                  explicacao#1\endcsname{#2}}%

% Comando que copia arquivos auxiliares
%\newcommand{\copiaArquivo}[2]{%
%            \newread\in%
%            \openin\in=#1%
%            \newwrite\out%
%            \immediate\openout\out #2%
            %\endlinechar %1\relax%-1%
%            \loop\unless\ifeof\in%
%              \readline\in to \l%
%              \immediate\write\out{\l}%
%            \repeat%
%            \immediate\closeout\out%
%            \immediate\closein\in%
            %\end%
%}%

% Comando que define o símbolo
\newcommand{\imprimeSimbolo}[1]{\ifcsdef{simbolo#1}{\expandafter\@nameuse{simbolo#1}}{\simboloIndefinido}}%

% Comando que redefine as unidades da nomenclatura
\newcommand{\nomUnit}[1]{\renewcommand{\nomentryend}{ \hspace*{\fill} #1 } }%

% Comando que seta a explicação
\newcommand{\setaExplicacao}[1]{\ifcsdef{explicacao#1}{\expandafter\csname%
                                explicacao#1\endcsname}{\explicacaoPadrao}}%

% Comando que edita a nomenclatura
\newcommand{\nomenclatureLine}[2]{\parbox[t]{0.75\textwidth}{#1}\nomUnit{#2}}%

% Comando que adiciona simbolo ao arquivo
\newcommand{\addSimboloAoArquivo}[1]{%
\immediate\openout\arquivoDeSimbolos=\arquivoAuxiliar%simbolos_mestrado.dat%
\write\arquivoDeSimbolos{\csname%
                         geraUmaNomenclatura{#1}%
                         \endcsname%
                         }%
\immediate\closein\arquivoDeSimbolos%
}%

% Comando que gera a lista de nomenclatura
\newcommand{\geraUmaNomenclatura}[1]{%
\nomenclature[\expandafter\csname%
              grupo#1\endcsname,%
              \expandafter\csname%
              letra#1\endcsname,%
              \expandafter\csname%
              ordem#1\endcsname%
              ]{\hspace{5pt}\expandafter\csname%
                simbolo#1\endcsname}{%
                \edef\variavelTemp{{\expandafter\csname unidade#1\endcsname}}%
                \expandafter\ifstrequal\variavelTemp{null}{%
                  \expandafter\csname explicacao#1\endcsname}{%
                    \nomenclatureLine{\setaExplicacao{#1}}{\expandafter\csname%
                    unidade#1\endcsname%
                    }%
                  }%
               }\xspace%
}%

% Define comando de simbolos que vão para nomenclatura
\newcommand{\comandoDoSimbolo}[1]{\expandafter\newcommand\csname%
                                  #1\endcsname[1][D]{%
                                    \ifstrequal{##1}{n}{%
                                      \expandafter\@nameuse{simboloNeg#1}%
                                      }{\expandafter\@nameuse{simbolo#1}%
                                        \@xspace\addSimboloAoArquivo{#1}%
                                        }%
                                    }%
                                  }%

% Comando que le o arquivo auxiliar com a lista de simbolos usados no texto
\newcommand{\atualizaNomenclatura}{%
  %\immediate\closein\arquivoDeSimbolos%
  \newread\arquivoDeSimbolos%
  %\IfFileExists{\arquivoAuxiliar}{true-branch}{false-branch}
  \immediate\openin\arquivoDeSimbolos=\arquivoAuxiliar%simbolos_mestrado.dat%
    \loop\unless\ifeof\arquivoDeSimbolos%
      \read\arquivoDeSimbolos to \fileline%
      \fileline%
    \repeat%
  \immediate\closein\arquivoDeSimbolos%
  %\copiaArquivo{\arquivoAuxiliar}{arquivo.dat}
}%

% Comando que imprime o símbolo todo
\newcommand{\imprimeSimboloTodo}[1]{\expandafter\@nameuse{grupo#1}%
            \expandafter\@nameuse{letra#1}%
            \expandafter\@nameuse{ordem#1}%
            \expandafter\@nameuse{simbolo#1}%
            \expandafter\@nameuse{simboloNeg#1}%
            \expandafter\@nameuse{explicacao#1}%
}%

\endinput

% Consertar a distância da unidade do último símbolo adicionado
% Consertar o parâmetro da unidade, pois quero colocar o comando da unidade
% Consertar o jeito que ele carrega automaticamente os símbolos
% Tentar fazer um teste automático para os símbolos
