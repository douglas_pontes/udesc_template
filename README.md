# UDESC Template #

Esse repositório contem o template Latex do trabalho de dissertação de mestrado para os cursos de pós-graduação da UDESC.

### O que contém esse repositório? ###

* Documento mestre do Latex que contém todos os inputs dos capítulos;
* Pasta document_class com a casse de documento e pacotes especiais da udesc;
* Demais pastas opcionais com exemplo de organização.

### Sugestões de melhoria do template ###

* Deve ser criada uma issue com as sugestões ou correções;
* A issue deve ser criada em: [Bitbucket](https://bitbucket.org/).